import Vue from 'vue'
import App from './App.vue'

import vuetify from './plugins/vuetify'

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Vuex from 'vuex'
import state from './store/state'
Vue.use(Vuex)

import moment from 'moment'
moment.locale('ru');


// import helloWorld from './components/HelloWorld.vue'
// import articlesPages from './pages/ArticlesPage.vue'
// import articlePage from './pages/ArticlePage.vue'
// import gallery from './pages/PhotoGallery.vue'
// import loginRegister from './pages/LoginRegister.vue'
// import profileView from './pages/ProfileView.vue'

import MainPage from './pages/MainPage.vue'
import SearchPage from './pages/SearchPage.vue'
import ProfilePage from './pages/ProfilePage.vue'
import FilmsPage from './pages/FilmsPage.vue'
import FilmPage from './pages/FilmPage.vue'

Vue.config.productionTip = false

const routes = [
  { path: '/', component: MainPage, name: 'main' },
  { path: '/search', component: SearchPage, name: 'search', props: true },
  { path: '/profile', component: ProfilePage },
  { path: '/films', component: FilmsPage },
  { path: '/films/:id', component: FilmPage, name: 'film', props: true },
]

const router = new VueRouter({
  routes
})

const store = new Vuex.Store(state)

Vue.filter('truncate', function (text, length, clamp) {
  clamp = clamp || '...';
  let node = document.createElement('div');
  node.innerHTML = text;
  let content = node.textContent;
  return content.length > length ? content.slice(0, length) + clamp : content;
});

Vue.filter('formatDate', function (value, type) {
  if (value) {
    if (type == 'DateAndTime') {
      return moment(String(value)).format('D.MM hh:mm');
    } else if (type == 'Date') {
      return moment(String(value)).format('D MMMM YYYY');
    }
  }
});

new Vue({
  render: h => h(App),
  vuetify,
  router,
  store
}).$mount('#app')
