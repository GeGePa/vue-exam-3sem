const axios = require("axios");

const state = {
  state: {
    count: 0,
    films: [],
    filtered_films: [],
    directors: [],
    actors: [],
    films_actors: [],
    schedule: [],
    search_result: [],
    comments_site: [],
    comments_film: [],
  },
  getters: {
    getFilms: (state) => {
      return state.films;
    },
    getFilteredFilms: (state) => {
      return state.filtered_films;
    },
    getDirectors: (state) => {
      return state.directors;
    },
    getActors: (state) => {
      return state.actors;
    },
    getFilmsActors: (state) => {
      return state.films_actors;
    },
    getSchedule: (state) => {
      return state.schedule;
    },
    getSearchResult: (state) => {
      return state.search_result;
    },
    getCommentsSite: (state) => {
      return state.comments_site;
    },
    getCommentsFilm: (state) => {
      return state.comments_film;
    },
  },
  mutations: {
      setFilms(state, films) {
      state.films = films;
    },
    setFilteredFilms(state, filtered_films) {
      state.filtered_films = filtered_films;
    },
    setDirectors(state, directors) {
      state.directors = directors;
    },
    setActors(state, actors) {
      state.actors = actors;
    },
    setFilmsActors(state, films_actors) {
      state.films_actors = films_actors;
    },
    setSchedule(state, schedule) {
      state.schedule = schedule;
    },
    setSearchResult(state, search_result) {
      state.search_result = search_result;
    },
    setCommentsSite(state, comments_site) {
      state.comments_site = comments_site;
    },
    setCommentsFilm(state, comments_film) {
      state.comments_film = comments_film;
    },
  },
  actions: {
            loadFilms(context) {
      axios
        .get("/main.json")
        .then((response) => {
          context.commit('setFilms', response.data.films);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    async loadFilmById(context, id) {
      return await axios
        .get("/main.json")
        .then((response) => {
          return response.data.films.filter(element => element.id == id)[0];
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    loadFilmsActors(context) {
      axios
        .get("/main.json")
        .then((response) => {
          context.commit('setFilmsActors', response.data.films_actors);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    loadActors(context) {
      axios
        .get("/main.json")
        .then((response) => {
          context.commit('setActors', response.data.actors);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    loadActorsByFilmId({ state, dispatch }, id) {
      let actorsList = [];

      dispatch('loadFilmsActors');
      dispatch('loadActors');

      function checkActors() {
        if (state.actors.length === 0 || state.films_actors.length === 0) {
          setTimeout(checkActors, 100);
          return;
        } else {
          state.films_actors.forEach(el => {
            if (el.film_id == id) {
              actorsList.push(state.actors.filter(element => element.id == el.actor_id)[0]);
            }
          });
        }
      }

      checkActors();

      return actorsList;
    },
    loadScheduleByFilmId(context, id) {
      let filmSchedule = [];
      axios
        .get("/main.json")
        .then((response) => {
          response.data.schedule.forEach(element => {
            if (element.film_id == id) {
              filmSchedule.push(element);
            }
          });
          context.commit('setSchedule', filmSchedule);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    getSearchResult(context, query) {
      let searchResult = [];

      axios
        .get("/main.json")
        .then((response) => {
          response.data.films.forEach(element => {
            if (element.title.toLowerCase().includes(query)) {
              searchResult.push(element);
            }
          })
          context.commit('setSearchResult', searchResult);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    async sortFilms(context, options) {
      let filmsList = [];
      if (context.state.filtered_films.length != 0) {
        filmsList = context.state.filtered_films;
      } else {
        if (context.state.films.length != 0) {
          filmsList = context.state.films;
        } else {
          await axios
            .get("/main.json")
            .then((response) => {
              filmsList = response.data.films;
            })
            .catch(function (error) {
              console.log(error);
            });
        }
      }
      // console.log(context.state.films, filmsList);
      // filmsList = context.state.filtered_films;

      let sortKey = options[0];
      let order = options[1];

      if (sortKey != 'default' && order == "asc") {
        filmsList.sort((a, b) => {
          let compare = 0;
          if (a[sortKey] > b[sortKey]) {
            compare = 1;
          } else if (b[sortKey] > a[sortKey]) {
            compare = -1;
          }
          return compare;
        });
      } else if (sortKey != 'default' && order == "desc") {
        filmsList.sort((a, b) => {
          let compare = 0;
          if (a[sortKey] < b[sortKey]) {
            compare = 1;
          } else if (b[sortKey] < a[sortKey]) {
            compare = -1;
          }
          return compare;
        });
      }

      // console.log('sort', filmsList);
      context.commit('setFilteredFilms', filmsList);
    },
    filterFilms(context, options) {
      let filteredFilms = [];

      let key = options[0];
      let requirement = options[1];

      if (key == 'default') {
        filteredFilms = context.state.films;
      } else {
        filteredFilms = context.state.films.filter(element => element[key] == requirement);
      }
      // console.log('filter', filteredFilms);
      context.commit('setFilteredFilms', filteredFilms);
      // context.dispatch("sortFilms", options);
    },
    loadCommentsSite(context) {
      axios
        .get("/main.json")
        .then((response) => {
          context.commit('setCommentsSite', response.data.comments_site);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    loadCommentsFilm(context, id) {
      axios
        .get("/main.json")
        .then((response) => {
          // console.log(response.data.comments_film.filter(element => element.film_id == id));
          context.commit('setCommentsFilm', response.data.comments_film.filter(element => element.film_id == id));
      })
        .catch(function (error) {
          console.log(error);
        });
    },
    postCommentsSite(context, form) {
      // axios
      //   .post("/", json)
      //   .then((response) => {
      // context.commit('setCommentsSite', response.data);
      //     console.log(response);
      //   })
      //   .catch(function (error) {
      //     console.log(error);
      //   });

      let comments = Array.from(context.state.comments_site);

      comments.push(form);

      context.commit('setCommentsSite', comments);
    },
    postCommentsFilm(context, form) {
      // axios
      //   .post("/", json)
      //   .then((response) => {
      // context.commit('setCommentsSite', response.data);
      //     console.log(response);
      //   })
      //   .catch(function (error) {
      //     console.log(error);
      //   });

      let comments = Array.from(context.state.comments_film);

      comments.push(form);

      context.commit('setCommentsFilm', comments);
    },
  },
}


export default state
